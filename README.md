HPC has attracted more attention lately by remote sensing applications due to the challenges posed by the increased amount of open data that are daily produced by Earth observation programs.
The unique parallel computing environments and programming techniques that are integrated in HPC systems might be able to solve large-scale problems such as the training of classification algorithms with large amounts of remote sensing data. 
This paper shows that the training of deep CNN can be efficiently performed in distributed fashion using parallel implementation techniques on HPC clusters.

Here the code used for the paper "Remote Sensing Big Data Classification with High Performance Distributed Deep Learning"
by R. Sedona et al. (https://doi.org/10.3390/rs11243056) is provided.

FOLDERS

1.  "data" folder, here you can find:
*  a link to the repository containing the full BigEarthNet dataset saved in the HDF5 format.
*  "arr_minmax.npy", a nummpy array needed to scale the input is provided as well. 

2. "resnet_code":
* "image_generator_hdf.py", a script containing the utils such as the data loader.
* "lars.py", definition and functions of the LARS optimizer.
* "resnet_finalversion_rmmax.py", definition of ResNet50 used for the project.
* "script_horovod_chunks_LARS_new.py", the main program .
* "submit_resnet_juwels_horovod.sh", example of job script to submit the training and evaluation. Here resources for computation are requested and modules loaded.

3. "dataset_code" folder: scripts used to create the HDF5 file from the original Sentinel2 tiles and BigEarthNet patches (http://bigearth.net/). Unless you want to create a new HDF5 file, you do not need to use the scripts listed below. The provided HDF5 file is already shuffled.
* "create_patches_paral_super.py", script to extract the patches from the tiles and save them into an HDF5 file.
* "shuffle_hdf.py", script to shuffle the HDF5 file.
* "submit_jureca_shuffle.sh", an example of job script to run the shuffling of the HDF5 file.

ARGUMENTS that can be passed to "script_horovod_chunks_LARS_new.py" for training:
* '--model', '-m', to select a model between between "super", "rgb" and "3branches"
* '--initialepoch', '-i', set initial epoch, if different from 0 resumes from checkpoint
* '--epochs', '-e', set number of epochs
* '--batchsize', '-b', set batch size, pay attention that this is the batch size for hvd each process
* '--wmepoch', '-w', set number of warmup epochs
* '--setupconf', '-s', configuration maxpooling first conv layer, at the moment leave it to "None" (default behaviour)
* '--pathimg', '-p', path to the HDF5 file containing data
* '--optimizer', '-o', choose between SGD and LARS optimizer
