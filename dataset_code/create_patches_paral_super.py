#import modules
import numpy as np
import pandas as pd
import h5py as h5
import os
import pickle
from osgeo import gdal
from sklearn.preprocessing import MultiLabelBinarizer
import mpi4py
from mpi4py import MPI
import h5py

def replace_tif(s):
   for r in (("MSIL1C","MSIL2A"), ("SAFE","tif")):
      s = s.replace(*r)
   return s


#compute cumulative sum of patches per worker and indreex tiles
def distribute_tiles(unique,n_proc):
    if len(unique)-n_proc>0:
        div = int(len(unique)/n_proc)
        idx = np.arange(n_proc)
        arr = np.zeros([n_proc])
        arr = unique.values[0:n_proc]
        for i in range(div):
           print(i)
           b = unique.values[(i+1)*n_proc:(i+2)*n_proc]
           arr = arr + np.flip(np.pad(b, (0,len(arr)-len(b)), 'constant', constant_values=(0)))
           print(arr)
           idx = np.vstack([idx,np.flip(np.pad(np.arange((i+1)*n_proc,(i+1)*n_proc+len(b),1), (0,len(arr)-len(b)), 'constant', constant_values=(-9999)))])
    else:
        arr = unique.values[0:len(unique)]
        idx = np.arange(len(unique))
    return arr, idx

#delete snow patches
def del_snow_clouds(patches):
    s_sn = pd.read_csv('patches_with_seasonal_snow.csv', header = None, squeeze=True)
    list_sn = []
    for s in s_sn:
        list_sn.append(patches[patches[0]==s].index.tolist())
        print(s)
    flat_list = [item for sublist in list_sn for item in sublist]
    patches=patches.drop(labels=flat_list)
    flat_list=[]
    del list_sn, s_sn
    #delete cloudy patches
    s_cl = pd.read_csv('patches_with_cloud_and_shadow.csv', header = None, squeeze=True)
    list_cl = []
    for s in s_cl:
        list_cl.append(patches[patches[0]==s].index.tolist())
        print(s)
    flat_list = [item for sublist in list_cl for item in sublist]
    patches=patches.drop(labels=flat_list)
    flat_list=[]
    del list_cl, s_cl
    return patches

print(__name__)
if __name__ == '__main__':
   #define list labels 
   print(__name__)
   comm = MPI.COMM_WORLD
   rank = comm.Get_rank()
   size = comm.Get_size()
   npatches = 0
   
   rootmlb = None
   mlb = MultiLabelBinarizer()
   arr = np.empty([])
   idx = np.empty([])
   list_tiles = []
   unique = pd.Series()
   rootarr = None
   rootidx = None
   rootlist_tiles = None
   rootunique = None
   print("rank: ", rank)
   if rank==0:
       print("rank: ", rank)
       list_labels=[["Agro-forestry areas"], ["Airports"], ["Annual crops associated with permanent crops"], ["Bare rock"], 
       ["Beaches, dunes, sands"], ["Broad-leaved forest"], ["Burnt areas"], ["Coastal lagoons"], ["Complex cultivation patterns"], ["Coniferous forest"], 
       ["Construction sites"], ["Continuous urban fabric"], ["Discontinuous urban fabric"], ["Dump sites"], ["Estuaries"], ["Fruit trees and berry plantations"],
       ["Green urban areas"], ["Industrial or commercial units"], ["Inland marshes"], ["Intertidal flats"],
       ["Land principally occupied by agriculture, with significant areas of natural vegetation"], ["Mineral extraction sites"], ["Mixed forest"],
       ["Moors and heathland"], ["Natural grassland"], ["Non-irrigated arable land"], ["Olive groves"], ["Pastures"], ["Peatbogs"], ["Permanently irrigated land"], 
       ["Port areas"], ["Rice fields"], ["Road and rail networks and associated land"], ["Salines"], ["Salt marshes"], ["Sclerophyllous vegetation"], ["Sea and ocean"], 
       ["Sparsely vegetated areas"], ["Sport and leisure facilities"], ["Transitional woodland/shrub"], ["Vineyards"], ["Water bodies"], ["Water courses"]]
       #initialize binarize
       rootmlb = MultiLabelBinarizer()
       lab = rootmlb.fit_transform(list_labels)
       print("rank: ", rank)
       #open 'tile_names_links.csv'
       """
       rootpatches = pd.read_csv("../prel_data/tile_names_links.csv", header = None, squeeze = True)
       rootpatches = del_snow_clouds(rootpatches)
      
       with open('list_patches.obj', 'wb') as fp:
          pickle.dump(rootpatches, fp)
       """
       print("rootpatches ")
       rootpatches = pickle.load(open('../prel_data/list_patches.obj', 'rb'))
       print("before value_counts")
       rootunique = rootpatches[1].value_counts()
       with open('unique.obj', 'wb') as fp:
          pickle.dump(rootunique, fp)

       print("before distribute_tiles")
       rootarr, rootidx = distribute_tiles(rootunique, size)
       print("after distribute_tiles")

       #open 'tile_names_links.csv'
       f = open('../prel_data/tile_names_links.csv', "r")
       lines = f.readlines()
       f.close()
       #get list of unique tiles
       rootlist_tiles = []
       for tile in lines:
          rootlist_tiles.append(tile.split(',')[1])

   else: 
      print("in else condition, rank:", rank)

   #broadcast rootlist_unique
   unique = comm.bcast(rootunique , root=0)
   #broadcast rootlist_tiles
   list_tiles = comm.bcast(rootlist_tiles, root=0)
   #broadcast multilabel binarizer
   print("before broadcast rootmlb, rank:", rank)
   mlb = comm.bcast(rootmlb, root=0)
   print("after broadcast rootmlb, rank:", rank)
   print("loading numpy arrays, rank:", rank)
   #load files with patches, cohord and classes
   patches=np.load("../prel_data/patches.npy", allow_pickle=True)
   classes=np.load("../prel_data/classes.npy", allow_pickle=True)
   coordinates=np.load("../prel_data/coordinates.npy", allow_pickle=True)
   print("finished numpy arrays, rank:", rank)
   
   print("bcast arr")
   arr = comm.bcast(rootarr , root=0)
   print("bcast idx")
   idx = comm.bcast(rootidx , root=0)

   print("rank:", rank)
   print("arr: ", arr)
   print("idx: ", idx)


   #load csv files with patches with seasonal snow, clouds and shadows
   s_sn = pd.read_csv('../prel_data/patches_with_cloud_and_shadow.csv', header = None, squeeze=True)
   s_cl = pd.read_csv('../prel_data/patches_with_seasonal_snow.csv', header = None, squeeze=True)
   s_conc = s_sn.tolist() + s_cl.tolist()

   #hdf5 file
   hdf5_path='/p/project/ccstdl/remote_sensing/rocco_sedona/Test_paral_super.h5'
   f = h5py.File(hdf5_path, 'w', driver='mpio', comm=comm)
   d_da = f.create_dataset("data_10m", shape=[519339, 120, 120, 12],dtype=np.uint16)
   d_cl = f.create_dataset("classes", shape=[519339, 43])
   d_san = f.create_dataset("sanity_check", shape=[519339])
   d_loc = f.create_dataset("loc", shape=[519339])

   #Each rank read a part of the matrix
   my_idx = idx[:, rank]
   sum_to_idx = np.sum(arr[np.arange(0, rank)])
   #sum_to_next_idx = np.sum(arr[np.arange(0, rank+1)])
   count = sum_to_idx

   #cycle over index of tile
   for iter_idx in my_idx:
      if iter_idx != -9999:
          
          loc = [i for i,val in enumerate(list_tiles) if val==unique.index[iter_idx]]
          print("loc: ", loc, "rank: ", rank)
          ds_or  = gdal.Open("/p/project/joaiml/remote_sensing/Data/tiles_2a_supres/" + replace_tif(unique.index[iter_idx]))
          for i in loc:
             pat = patches[i]
             if pat not in s_conc:
                 cl = classes[i]
                 co = coordinates[i]
                 dt_or = gdal.Translate('../temp_patch/output' + str(i) + '_10m_' + str(rank) + '.tif', ds_or, projWin = [co['ulx'], co['uly'], co['lrx'], co['lry']])
                 #save datacube of the patches with classes as we like :)
                 datacube = np.empty([120, 120, 12], dtype=np.uint16)
		 
                 #10m bands
                 for j in range(1,13):
                    band = dt_or.GetRasterBand(j)
                    data = band.ReadAsArray()
                    datacube[:,:,j-1] = data
                 dt_or = None
                 #print(datacube.shape)
                 d_da[count,:,:,:] = datacube
                 d_loc[count] = i
                 print('no couds, shadows and seasonal snow')   
                 print('count=%d', count)
                 d_cl[count, :] = mlb.transform([cl])
                 #d_san is put to 1 if the saved output is valid
                 d_san[count] = 1
                 count = count + 1
                 print('finito un ciclo ')
      
      #clean the directory
      #temporary .tiff folder path
      ld = os.listdir('../temp_patch/')
      for l in ld:
         if l.split('.')[1]=='tif':
            if l.split('.')[0].split('_')[2] == str(rank):
               os.remove('../temp_patch/'+l)
               print("removed file ", l)
   
   #close the hdf file
   f.close()