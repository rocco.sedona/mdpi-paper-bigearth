#!/bin/bash -x
#SBATCH --account=cstdl
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=create_pat-out.%j
#SBATCH --error=create_pat-err.%j
#SBATCH --time=1:00:00
#SBATCH --partition=batch

ml use /usr/local/software/jureca/OtherStages

module load Stages/Devel-2019a GCC/8.3.0 ParaStationMPI
module load h5py/2.9.0-Python-3.6.8

srun python shuffle_hdf.py