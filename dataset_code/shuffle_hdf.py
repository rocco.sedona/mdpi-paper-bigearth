# -*- coding: utf-8 -*-
"""
Created on Sat Sep  7 09:07:18 2019

@author: rocco
"""

import numpy as np
import h5py
import random
import os

#data = h5py.File('Test_paral_complete_or.h5', 'r')
data = h5py.File('Test_upsampled.h5', 'r')
out = h5py.File('Test_mid_shuffled.h5', 'w')
dim_dataset = data['classes'].shape[0]
d_da = out.create_dataset("data_10m", shape=[dim_dataset, 120, 120, 4],dtype=np.uint16)
d_da_super = out.create_dataset("data_super", shape=[dim_dataset, 120, 120, 12],dtype=np.uint16)
d_da_20m = out.create_dataset("data_20m", [dim_dataset, 60, 60, 6],dtype=np.uint16)
d_da_60m = out.create_dataset("data_60m", shape=[dim_dataset, 20, 20, 2],dtype=np.uint16)

d_cl = out.create_dataset("classes", shape=[dim_dataset, 43])
d_san = out.create_dataset("sanity_check", shape=[dim_dataset])
d_loc = out.create_dataset("loc", shape=[dim_dataset])

#out_final = h5py.File('Test_shuffled.h5', 'w')
out_final = h5py.File('Test_upsampled_shuffled.h5', 'w')
d_daf = out_final.create_dataset("data_10m", shape=[dim_dataset, 120, 120, 4],dtype=np.uint16)
d_daf_super = out_final.create_dataset("data_super", shape=[dim_dataset, 120, 120, 12],dtype=np.uint16)
d_daf_20m = out_final.create_dataset("data_20m", [dim_dataset, 60, 60, 6],dtype=np.uint16)
d_daf_60m = out_final.create_dataset("data_60m", shape=[dim_dataset, 20, 20, 2],dtype=np.uint16)

d_clf = out_final.create_dataset("classes", shape=[dim_dataset, 43])
d_sanf = out_final.create_dataset("sanity_check", shape=[dim_dataset])
d_locf = out_final.create_dataset("loc", shape=[dim_dataset])

#first shuffling
indexes = np.arange(data['data_super'].shape[0])
n_it = 5
dim_chunk =  int(indexes.shape[0]/n_it)
for i in range(0, n_it):
   if i != (n_it-1):
      sub_idx = indexes[i*dim_chunk:(i+1)*dim_chunk]
   else:
      sub_idx = indexes[i*dim_chunk:indexes.shape[0]]
   indices = np.arange(sub_idx.shape[0])
   np.random.shuffle(indices)
   for key in data.keys():
       d=data[key][sub_idx.tolist()]
       d=d[indices]
       out[key][i*dim_chunk:i*dim_chunk+sub_idx.shape[0]] = d
       #print(i*dim_chunk,i*dim_chunk+sub_idx.shape[0])

data.close()

#second reshuffling
#n_it = 10000
n_it = 200000
indexes = np.arange(out['data_super'].shape[0])
dim_chunk =  int(indexes.shape[0]/n_it)
count = 0 

for i in random.sample(range(0, n_it), n_it):
   if i != (n_it-1):
      sub_idx = indexes[i*dim_chunk:(i+1)*dim_chunk]
   else:
      sub_idx = indexes[i*dim_chunk:indexes.shape[0]]
   indices = np.arange(sub_idx.shape[0])
   np.random.shuffle(indices)
   for key in out.keys():
       d=out[key][sub_idx.tolist()]
       d=d[indices]
       if i != (n_it-1):
          out_final[key][count*dim_chunk:count*dim_chunk+sub_idx.shape[0]] = d
       else:
          out_final[key][i*dim_chunk:i*dim_chunk+sub_idx.shape[0]] = d
   if i != (n_it-1):
      count = count + 1 
            
out.close()
os.remove('Test_mid_shuffled.h5')
out_final.close()
