# -*- coding: utf-8 -*-
"""
Created on 4/10/2019

@author: rocco
"""
#load libraries
import numpy as np
import sys
import h5py
import pickle
import os
import math
import argparse
from sklearn.metrics import precision_recall_fscore_support as score
from resnet_finalversion_rmmax import ResNet50, ResNet50_3branches
from image_generator_hdf import image_generator, image_generator_chunks, test_generator, get_y_true, image_generator_3branches_chunks, test_generator_3branches
from keras import optimizers, callbacks
from sklearn.model_selection import train_test_split
import horovod.keras as hvd
from keras import backend as K
import tensorflow as tf
from tensorflow.python.client import timeline
import time
import mpi4py
import keras 
from keras.models import model_from_json
from keras.models import load_model
import matplotlib.pyplot as plt
from lars import LARS

mpi4py.rc.initialize = False
#add path to scripts if needed
#sys.path.insert(0, '\\..\\data')

#definition of custom binary cross-entropy 
def binary_crossentropy_custom(y_true, y_pred):
    return K.binary_crossentropy(y_true, y_pred) * mask

#definition class TimeHistory
class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)

#definition class to get lr
class GetLr(keras.callbacks.Callback):
   def on_epoch_end(self, epoch, logs=None):
      lr = float(K.get_value(self.model.optimizer.lr))
      print("rank: ", hvd.rank(), ", learning rate:", lr)

#definition function step_decay implementing also warmup

def step_decay(epoch):
   base_lr = 0.1
   base_batch_size = 256
   eff_batch_size = hvd.size() * int(args.batchsize)
   initial_lrate = base_lr * eff_batch_size / base_batch_size
   wmepoch = int(args.wmepoch)
   drop = 10

   if epoch<wmepoch:
        lrate = (initial_lrate/hvd.size())*(((hvd.size()-1)/wmepoch)*epoch + 1)
   elif epoch<30:
       lrate = initial_lrate
   elif epoch<60:
       lrate = initial_lrate / drop 
   elif epoch<80:
       lrate = initial_lrate / (drop**2)
   else:
       lrate = initial_lrate / (drop**3)
   
   #print LR to be set
   if hvd.rank() == 0:
      print("LR: ", lrate," wmepoch: ", wmepoch," epoch: ", epoch)

   return lrate
"""
#definition function for polynomial decay
class LearningRateDecay:
	def plot(self, epochs=100, title="Learning Rate Schedule"):
		# compute the set of learning rates for each corresponding
		# epoch
		lrs = [self(i) for i in epochs]
 
		# the learning rate schedule
		plt.style.use("ggplot")
		plt.figure()
		plt.plot(epochs, lrs)
		plt.title(title)
		plt.xlabel("Epoch #")
		plt.ylabel("Learning Rate")

class PolynomialDecay(LearningRateDecay):
	def __init__(self, maxEpochs=100, power=5.0):
		# store the maximum number of epochs, base learning rate,
		# and power of the polynomial
		self.maxEpochs = maxEpochs
		self.power = power
 
	def __call__(self, epoch):
                base_lr = 0.0125
                base_batch_size = 32
                eff_batch_size = hvd.size() * int(args.batchsize)
                #from previous work
                #initial_lrate = base_lr * eff_batch_size / base_batch_size
                #initial_lrate = base_lr * 8000 / base_batch_size

                #initial_lrate from You2007 technical report on LARS optimizer
                if eff_batch_size == 8192:
                   initial_lrate = 0.6
                elif eff_batch_size == 16384:
                   initial_lrate = 2.5
                elif eff_batch_size == 32768:
                   initial_lrate = 2.9
                else:
                   initial_lrate = 0.2

                wmepoch = int(args.wmepoch)

        		# compute the new learning rate based on polynomial decay
                decay = (1 - ((epoch - wmepoch)/ float(self.maxEpochs))) ** self.power
                alpha= initial_lrate * decay
                if epoch < wmepoch:
                    alpha = (initial_lrate/hvd.size())*(((hvd.size()-1)/wmepoch)*epoch + 1)
                return float(alpha)
"""
if __name__ == '__main__':
    # Horovod: initialize Horovod.
    hvd.init()
    #mpi comm, use MPI after having initialized Horovod
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(hvd.local_rank())
    K.set_session(tf.Session(config=config))
    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    run_metadata= tf.RunMetadata()

    #load normalization array
    arr_norm = np.load("../data/arr_minmax.npy")
    #set parser 
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', '-m', help="select a model", type= str, default= "super")
    parser.add_argument('--initialepoch', '-i', help="set initial epoch", type= str, default= "0")
    parser.add_argument('--epochs', '-e', help="set number of epochs", type= str, default= "100")
    parser.add_argument('--batchsize', '-b', help="set batch size", type= str, default= "256")
    parser.add_argument('--wmepoch', '-w', help="wmepoch", type= str, default= "256")
    parser.add_argument('--setupconf', '-s', help="configuration maxpooling first conv layer", type= str, default= "None")
    parser.add_argument('--pathimg', '-p', help="path to file containing data", type= str, default= "/p/project/ccstdl/remote_sensing/rocco_sedona/Test_paral_complete.h5")
    parser.add_argument('--optimizer', '-o', help="choose between SGD and LARS", type= str, default= "LARS")


    args=parser.parse_args()
    #path to hdf5 file
    #comm.Barrier()
    try:
        path2img = args.pathimg
    except NameError:
        print("Invalid path to data file")
    new_file = h5py.File(path2img , "r")
    N_SAMPLES = new_file['classes'].shape[0]
    NUM_TRN_IMAGES = 0
    NUM_VAL_IMAGES = 0
    NUM_TST_IMAGES = 0  
    rootNUM_TRN_IMAGES = None
    rootNUM_VAL_IMAGES = None
    rootNUM_TST_IMAGES = None
    rootX_train = None
    rootX_val = None
    rootX_test = None
    print("size hvd", hvd.size()) 
    print("rank hvd", hvd.rank())

    if hvd.rank() == 0:
       x = np.arange(N_SAMPLES)
       rootX_train = np.arange(0, N_SAMPLES*0.6).astype(int)
       rootX_val = np.arange(N_SAMPLES*0.6, N_SAMPLES*0.8).astype(int)
       rootX_test = np.arange(N_SAMPLES*0.8, N_SAMPLES).astype(int)
       rootNUM_TRN_IMAGES = len(rootX_train)
       rootNUM_VAL_IMAGES = len(rootX_val)
       rootNUM_TST_IMAGES = len(rootX_test)
 
    NUM_TRN_IMAGES=comm.bcast(rootNUM_TRN_IMAGES, root=0)    
    NUM_VAL_IMAGES=comm.bcast(rootNUM_VAL_IMAGES, root=0)
    NUM_TST_IMAGES=comm.bcast(rootNUM_TST_IMAGES, root=0)
    
    X_train = np.empty(NUM_TRN_IMAGES)
    X_val = np.empty(NUM_VAL_IMAGES)
    X_test = np.empty(NUM_TST_IMAGES)
    
    X_train = comm.bcast(rootX_train, root=0)    
    X_val = comm.bcast(rootX_val, root=0)
    X_test = comm.bcast(rootX_test, root=0)
    print("X_train: ", X_train)
    print("X_train shape: ", X_train.shape[0])

    #set directory
    try:
       if args.model=='super':
          num_channels = 12
          directory = './resnet_experiment_superes'
       if args.model=='rgb':
          num_channels = 3
          directory = './resnet_experiment_rgb'
       if args.model=='rgb_imagenet':
          num_channels = 3
          directory = './resnet_experiment_rgb_imagenet'
       if args.model=='t_branches':
          directory = './resnet_experiment_3branches'
    except NameError:
        print("Invalid model name")

    #from checkpoint
    try:
       if args.initialepoch=='0':
          initial_epoch = 0
          checkpoint_path = None
       else:
          initial_epoch = args.initialepoch
          print(initial_epoch)
          print(type(initial_epoch))
          checkpoint_path = directory + "/checkpoints/checkpoint-" + initial_epoch + ".h5"
          initial_epoch = int(checkpoint_path.split("-")[1].split(".")[0])
    except NameError:
       print("Invalid initial epoch")

    #parameters of the model
    BATCH_SIZE = int(args.batchsize)
    STEPS = NUM_TRN_IMAGES // BATCH_SIZE
    VAL_STEPS = NUM_VAL_IMAGES // BATCH_SIZE
    try:
        EPOCHS = int(args.epochs)
    except NameError:
        print("Invalid epoch argument")
    epoch = initial_epoch
    #instantiate scheduler
    try:
       if args.optimizer=='LARS':
          #schedule = PolynomialDecay(maxEpochs=EPOCHS, power=2)
          #lrate = keras.callbacks.LearningRateScheduler(schedule)
          lrate = keras.callbacks.LearningRateScheduler(step_decay)
       elif args.optimizer=='SGD':
          lrate = keras.callbacks.LearningRateScheduler(step_decay)
    except NameError:
        print("Invalid optimizer argument") 

    #load model
    if checkpoint_path == None:
       try:
          if args.model=='super':
             #lra = 0.01
             #num_channels = 12
             model = ResNet50(weights = None, channels = num_channels, setup_conf=args.setupconf)
             #directory = './resnet_experiment_superes'
          if args.model=='rgb':
             #lra = 0.01
             #num_channels = 3
             model = ResNet50(weights = None, channels = num_channels)
             #directory = './resnet_experiment_rgb'
          if args.model=='rgb_imagenet':
             #num_channels = 3
             #lra = 0.00001
             model = ResNet50(weights = "imagenet", channels = num_channels)
             #directory = './resnet_experiment_rgb_imagenet'
          if args.model=='t_branches':
             #lra = 0.01
             model = ResNet50_3branches()
             #directory = './resnet_experiment_3branches'

       except NameError:
          print("Invalid model name")

       #instantiate model 
       if args.optimizer=='LARS':
          #opt = LARS(lr=schedule(0), weight_decay=0, momentum=0.9, nesterov=True)
          opt = LARS(lr=step_decay(0), weight_decay=0, momentum=0.9, nesterov=True)
       elif args.optimizer=='SGD':
          opt = optimizers.SGD(lr=step_decay(0), decay=0, momentum=0.9, nesterov=True)
          
       # Horovod: add Horovod Distributed Optimizer.
       opt = hvd.DistributedOptimizer(opt)
       model.compile(loss='binary_crossentropy', optimizer=opt , metrics=['mae', 'acc'])
        
    getlr_callback = GetLr()
    callbacks = [
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    hvd.callbacks.BroadcastGlobalVariablesCallback(0), 
    hvd.callbacks.MetricAverageCallback(),
    #warmup in the first 5 epochs to gradually increase the learning rate
    #hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=wm_epoch, verbose=1), 
    lrate,
    getlr_callback
    ]
    #load model if there exists checkpoint (done manually here!)
    # Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
    if hvd.rank() == 0:
       callbacks.append(keras.callbacks.ModelCheckpoint(directory + '/checkpoints/checkpoint-{epoch}.h5', period=10))
       #create directory to save results if not existent
       callbacks.append(keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0,
                          write_graph=True, write_images=False))
       time_callback = TimeHistory()
       callbacks.append(time_callback)
       #callbacks.append(WandbCallback())
       if not os.path.exists(directory + "/checkpoints"):
          os.makedirs(directory + "/checkpoints")
    if checkpoint_path is not None:
       # Load model:
       model = hvd.load_model(checkpoint_path)
       # Finding the epoch index from which we are resuming
       #initial_epoch = int(checkpoint_path.split("-")[1].split(".")[0])
    """
    else:
       initial_epoch = 0
    """
    #fit model
    # steps_per_epoch = STEPS // hvd.size()---> remember to divide the n. of steps also by the number of workers (i.e. GPUs)
    #timer
    if hvd.rank() == 0:
       start = time.perf_counter()
    if args.model!='t_branches':
       history = model.fit_generator(generator = image_generator_chunks(file = new_file, ind = X_train, batch_size = BATCH_SIZE, rank = hvd.rank(), n_channels = num_channels, arr_norm=arr_norm),\
							      validation_steps=VAL_STEPS // hvd.size(), \
                                                              validation_data = image_generator_chunks(file = new_file, ind = X_train, batch_size = BATCH_SIZE, rank = hvd.rank(), n_channels = num_channels, arr_norm=arr_norm),\
							      steps_per_epoch = STEPS // hvd.size(), epochs = EPOCHS, callbacks=callbacks, initial_epoch = initial_epoch)
    else: 
       history = model.fit_generator(generator = image_generator_3branches_chunks(file = new_file, ind = X_train, batch_size = BATCH_SIZE, rank = hvd.rank(), arr_norm=arr_norm),\
							      validation_steps=VAL_STEPS // hvd.size(), \
                                                              validation_data=image_generator_3branches_chunks(file = new_file, ind = X_val, batch_size = BATCH_SIZE, rank = hvd.rank(), arr_norm=arr_norm),
							      steps_per_epoch = STEPS // hvd.size(), epochs = EPOCHS, callbacks=callbacks, initial_epoch = initial_epoch)

    
    #predict only on the first GPU
    if hvd.rank() == 0:
       elapsed = time.perf_counter() - start
       print('Training time: %.3f seconds.' % elapsed)
       #setup  3branches
       if args.model!='t_branches':
          predictions = model.predict_generator(generator = test_generator(file = new_file, ind = X_test, n_channels = num_channels, arr_norm=arr_norm), steps = NUM_TST_IMAGES)
       #other setup than 3branches
       else:
          predictions = model.predict_generator(generator = test_generator_3branches(file = new_file, ind = X_test, arr_norm=arr_norm), steps = NUM_TST_IMAGES)
       #saving some vars to files
       predictions_class = np.where(predictions > 0.4, 1, 0)
       np.save(directory + "/pred.npy", predictions)
       np.save(directory + "/pred_class.npy", predictions_class)
       with open(directory + '/history.pickle', 'wb') as file_pi:
           pickle.dump(history.history, file_pi)
       target_true = get_y_true(new_file, X_test)
       np.save(directory + "/true_class.npy", target_true)
       elapsed = time.perf_counter() - start
       print('Time incl test: %.3f seconds.' % elapsed)
       print('Time_epoch: {}'.format(np.mean(time_callback.times)))

       #computing statistics
       precision, recall, fscore, support = score(target_true, predictions_class)
       #print metrics
       print('precision: {}'.format(precision))
       print('recall: {}'.format(recall))
       print('fscore: {}'.format(fscore))
       print('support: {}'.format(support))
       #computing statistics
       precision, recall, fscore, support = score(target_true, predictions_class, average='micro')
       #print metrics
       print('precision: {}'.format(precision))
       print('recall: {}'.format(recall))
       print('fscore: {}'.format(fscore))
       print('support: {}'.format(support))
       # serialize model to JSON
       #  the keras model which is trained is defined as 'model' in this example
       model_json = model.to_json()
       with open(directory + "/model_num.json", "w") as json_file:
          json_file.write(model_json)
       # serialize weights to HDF5
       model.save_weights(directory + "/model_num.h5")

    #close hdf5 file
    new_file.close()
