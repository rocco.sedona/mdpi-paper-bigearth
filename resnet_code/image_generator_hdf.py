# -*- coding: utf-8 -*-
"""
Created on 12/06/2019

@author: rocco
The most important function in this file is the "image_generator_chunks". This is the data loader from the HDF5 file.
Further investigations should follow to understand if it causes a bottleneck.
"""
import numpy as np
import pandas as pd
from scipy import ndimage
import os
import h5py
from scipy.stats import beta

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

def unison_shuffled_copies_rgb(a, b, c, d):
    assert len(a) == len(b) == len(c) == len(d)
    p = np.random.permutation(len(a))
    return a[p], b[p], c[p], d[p]


def data_aug(batch):
    #vertical flip
    if np.random.choice(2, 1)[0]:
        batch = np.flip(batch,2)
    """
    #horizontal flip
    if np.random.choice(2, 1)[0]:
        batch = np.flip(batch,2)
    """
    #rotate 90 degrees
    rd = np.random.choice(4, 1)[0]
    if rd == 0:
        batch = np.rot90(batch, axes=[1,2])
    elif rd == 1:
        batch = np.rot90(batch, 2, axes=[1,2])
    elif rd == 2:
        batch = np.rot90(batch, 3, axes=[1,2])
    return batch

def mix_up(batch_x, batch_y):
    batch_x_shuffled , batch_y_shuffled = unison_shuffled_copies(batch_x, batch_y)
    a = 0.5
    b = 0.5
    r = beta.rvs(a, b, size=batch_x.shape[0])
    #import pdb; pdb.set_trace()
    batch_x = (r[:, np.newaxis, np.newaxis, np.newaxis]*batch_x) + (1-r[:, np.newaxis, np.newaxis, np.newaxis])*batch_x_shuffled
    batch_y = (r[:, np.newaxis]*batch_y) + (1-r[:, np.newaxis])*batch_y_shuffled
    #print("batch sizes: ", batch_x.shape, batch_y.shape)
    return(batch_x, batch_y)

def mix_up_rgb(batch_x_10m, batch_x_20m, batch_x_60m, batch_y):
    batch_x_10m_shuffled, batch_x_20m_shuffled, batch_x_60m_shuffled, batch_y_shuffled = unison_shuffled_copies_rgb(batch_x_10m, batch_x_20m, batch_x_60m, batch_y)
    a = 0.5
    b = 0.5
    r = beta.rvs(a, b, size=batch_x_10m.shape[0])
    #import pdb; pdb.set_trace()
    batch_x_10m = (r[:, np.newaxis, np.newaxis, np.newaxis]*batch_x_10m) + (1-r[:, np.newaxis, np.newaxis, np.newaxis])*batch_x_10m_shuffled
    batch_x_20m = (r[:, np.newaxis, np.newaxis, np.newaxis]*batch_x_20m) + (1-r[:, np.newaxis, np.newaxis, np.newaxis])*batch_x_20m_shuffled
    batch_x_60m = (r[:, np.newaxis, np.newaxis, np.newaxis]*batch_x_60m) + (1-r[:, np.newaxis, np.newaxis, np.newaxis])*batch_x_60m_shuffled
    batch_y = (r[:, np.newaxis]*batch_y) + (1-r[:, np.newaxis])*batch_y_shuffled
    #print("batch sizes: ", batch_x.shape, batch_y.shape)
    return(batch_x_10m, batch_x_20m, batch_x_60m, batch_y)
 
    
def image_generator(file, ind, batch_size, rank, n_channels):
    #ir = 0 
    dataset="data_super"
    #dataset="data" #old hdf file
    while True:
          #np.random.seed(ir + rank)
          # Select files (paths/indices) for the batch
          subind = np.random.choice(ind, batch_size)
          batch_x = np.empty([0,120,120, n_channels])
          batch_y = np.empty([0,43])
          if n_channels == 12:
             for i in subind:
                batch_x = np.vstack([batch_x, file[dataset][i].reshape(1,120,120, n_channels)])
                batch_y = np.vstack([batch_y, file['classes'][i].reshape(1,43)])
          else:
             for i in subind:
                batch_x = np.vstack([batch_x, file[dataset][i, :, :, 1:4].reshape(1,120,120, n_channels)])
                batch_y = np.vstack([batch_y, file['classes'][i].reshape(1,43)])

          #ir = ir + 100
          yield( batch_x, batch_y )

#function to load the data from HDF5 file in chunks. At the moment, the chunk size is set to be 4 times larger than the batch_size
def image_generator_chunks(file, ind, batch_size, rank, n_channels, arr_norm):
    dataset = "data_super"
    large_batch_size = batch_size*4
    ind = ind[:-large_batch_size]
    batch_x = np.empty([large_batch_size,120,120, n_channels])
    batch_y = np.empty([large_batch_size,43])
    j=0
    while True:
          small_batch_x = np.empty([batch_size,120,120, n_channels])
          small_batch_y = np.empty([batch_size,43])
          if j%4==0:
             i = np.random.choice(ind)
             if n_channels == 12:
                   #biliniear interpolation
                   batch_10m = (file["data_10m"][i:i+large_batch_size]-arr_norm[2, 0:4])/arr_norm[3, 0:4]
                   batch_20m = (file["data_20m"][i:i+large_batch_size]-arr_norm[2, 4:10])/arr_norm[3, 4:10]
                   batch_60m = (file["data_60m"][i:i+large_batch_size]-arr_norm[2, 10:12])/arr_norm[3, 10:12]
                   batch_20m = ndimage.zoom(batch_20m, [1,2,2,1], order=0)
                   batch_60m = ndimage.zoom(batch_60m, [1,6,6,1], order=0)
                   batch_x = np.concatenate((batch_10m,batch_20m,batch_60m), axis=3)
                   batch_x = data_aug(batch_x)
                   batch_y = file['classes'][i:i+large_batch_size]
             else:
                   batch_x= np.flip(data_aug((file[dataset][i:i+large_batch_size,:,:,0:3]-arr_norm[2, 0:3])/arr_norm[3, 0:3]), 3)
                   #batch_x= data_aug((file[dataset][i:i+batch_size,:,:,0:3]-arr_norm[2, 0:3])/arr_norm[3, 0:3])
                   batch_y = file['classes'][i:i+large_batch_size]
             batch_x, batch_y = mix_up(batch_x, batch_y)
          #load small_batch from large_batch
          small_batch_x = batch_x[(j%4)*batch_size:((j%4)+1)*batch_size,:,:,:]
          small_batch_y = batch_y[(j%4)*batch_size:((j%4)+1)*batch_size]
          j=j+1   
          yield( small_batch_x, small_batch_y )



"""
def image_generator_chunks(file, ind, batch_size, rank, n_channels, arr_norm):
    dataset = "data_super"
    ind = ind[:-batch_size]
    while True:
          i = np.random.choice(ind)
          batch_x = np.empty([batch_size,120,120, n_channels])
          batch_y = np.empty([batch_size,43])
          
          if n_channels == 12:
                '''
                #commented super-resolved
                batch_x = data_aug((file[dataset][i:i+batch_size]-arr_norm[2, 12:])/arr_norm[3, 12:])
                batch_y = file['classes'][i:i+batch_size]
                '''
                #biliniear interpolation
                batch_10m = (file["data_10m"][i:i+batch_size]-arr_norm[2, 0:4])/arr_norm[3, 0:4]
                batch_20m = (file["data_20m"][i:i+batch_size]-arr_norm[2, 4:10])/arr_norm[3, 4:10]
                batch_60m = (file["data_60m"][i:i+batch_size]-arr_norm[2, 10:12])/arr_norm[3, 10:12]
                batch_20m = ndimage.zoom(batch_20m, [1,2,2,1], order=0)
                batch_60m = ndimage.zoom(batch_60m, [1,6,6,1], order=0)
                batch_x = np.concatenate((batch_10m,batch_20m,batch_60m), axis=3)
                batch_x = data_aug(batch_x)
                batch_y = file['classes'][i:i+batch_size]
          else:
                batch_x= np.flip(data_aug((file[dataset][i:i+batch_size,:,:,0:3]-arr_norm[2, 0:3])/arr_norm[3, 0:3]), 3)
                #batch_x= data_aug((file[dataset][i:i+batch_size,:,:,0:3]-arr_norm[2, 0:3])/arr_norm[3, 0:3])
                batch_y = file['classes'][i:i+batch_size]
          #added mixup
          batch_x, batch_y = mix_up(batch_x, batch_y)
          yield( batch_x, batch_y )


def image_generator_chunks(file, ind, batch_size, rank, n_channels, arr_norm):
    dataset = "data_super"
    dim_mb = 15
    ind = ind[:-dim_mb*batch_size]
    count = 0
    while True:
          count_modulo = count%dim_mb
          #print("modulo:", count_modulo)
          if count_modulo == 0:
             i = np.random.choice(ind)
             maxi_batch_x = file[dataset][i:i+dim_mb*batch_size]
             maxi_batch_y = file["classes"][i:i+dim_mb*batch_size]

          batch_x = np.empty([batch_size,120,120, n_channels])
          batch_y = np.empty([batch_size,43])
          if n_channels == 12:
                batch_x = data_aug((maxi_batch_x[count_modulo*batch_size:(count_modulo+1)*batch_size]-arr_norm[2, 12:])/arr_norm[3, 12:])
                batch_y = maxi_batch_y[count_modulo*batch_size:(count_modulo+1)*batch_size]
          else:
                batch_x= data_aug((file[dataset][i:i+batch_size,:,:,0:3]-arr_norm[2, 0:3])/arr_norm[3, 0:3])
                batch_y = file['classes'][i:i+batch_size]
          count = count + 1
          yield( batch_x, batch_y )
"""

def image_generator_ram(dat, lab, batch_size, rank, n_channels, arr_norm):
    ind = np.arange(0, lab.shape[0])
    ind = ind[:-batch_size]
    while True:
          i = np.random.choice(ind)
          batch_x = np.empty([batch_size,120,120, n_channels])
          batch_y = np.empty([batch_size,43])
          if n_channels == 12:
                batch_x = data_aug((dat[i:i+batch_size]-arr_norm[2, 12:])/arr_norm[3, 12:])
                batch_y = lab[i:i+batch_size]
          else:
                batch_x= data_aug((dat[i:i+batch_size,:,:,0:3]-arr_norm[2, 0:3])/arr_norm[3, 0:3])
                batch_y = lab[i:i+batch_size]
          
          yield( batch_x, batch_y )


def image_generator_3branches(file, ind, batch_size, rank):
    #ir = 0 
    d_10m="data_10m"
    d_20m="data_20m"
    d_60m="data_60m"
    while True:
          #np.random.seed(ir + rank)
          # Select files (paths/indices) for the batch
          subind = np.random.choice(ind, batch_size)
          batch_x_10m = np.empty([0,120,120, 4])
          batch_x_20m = np.empty([0,60,60, 6])
          batch_x_60m = np.empty([0,20,20, 2])
          batch_y = np.empty([0,43])
          for i in subind:
             batch_x_10m = np.vstack([batch_x_10m, file[d_10m][i].reshape(1,120,120, 4)])
             batch_x_20m = np.vstack([batch_x_20m, file[d_20m][i].reshape(1,60,60, 6)])
             batch_x_60m = np.vstack([batch_x_60m, file[d_60m][i].reshape(1,20,20, 2)])
             batch_y = np.vstack([batch_y, file['classes'][i].reshape(1,43)])

          #ir = ir + 100
          yield( [batch_x_10m,batch_x_20m,batch_x_60m], batch_y )

def image_generator_3branches_chunks(file, ind, batch_size, rank, arr_norm):
    d_10m="data_10m"
    d_20m="data_20m"
    d_60m="data_60m"
    ind = ind[:-batch_size]
    while True:
          i = np.random.choice(ind)
          batch_x_10m = np.empty([batch_size,120,120, 4])
          batch_x_20m = np.empty([batch_size,60,60, 6])
          batch_x_60m = np.empty([batch_size,20,20, 2])
          batch_y = np.empty([batch_size,43])
          batch_x_10m= data_aug((file[d_10m][i:i+batch_size]-arr_norm[2, 0:4])/arr_norm[3, 0:4])
          batch_x_20m= data_aug((file[d_20m][i:i+batch_size]-arr_norm[2, 4:10])/arr_norm[3, 4:10])
          batch_x_60m= data_aug((file[d_60m][i:i+batch_size]-arr_norm[2, 10:12])/arr_norm[3, 10:12])
          batch_y = file['classes'][i:i+batch_size]
          #added mixup
          batch_x_10m, batch_x_20m, batch_x_60m, batch_y = mix_up_rgb(batch_x_10m, batch_x_20m, batch_x_60m, batch_y)

          yield( [batch_x_10m,batch_x_20m,batch_x_60m], batch_y )

def test_generator(file, ind, n_channels, arr_norm):
    i = 0
    dataset="data_super"
    #dataset="data" #old hdf file
    while True:

          # Load test sample (paths/indices)
          sample_x = np.empty([1,120,120, n_channels])
          if n_channels == 12:
             '''
             #super-resolution
             #sample_x = (file[dataset][ind[i]].reshape(1,120,120, n_channels)-arr_norm[2, 12:])/arr_norm[3, 12:]
             '''
             #bilinear interpolation
             batch_10m = (file["data_10m"][ind[i]].reshape(1,120,120, 4)-arr_norm[2, 0:4])/arr_norm[3, 0:4]
             batch_20m = (file["data_20m"][ind[i]].reshape(1,60,60, 6)-arr_norm[2, 4:10])/arr_norm[3, 4:10]
             batch_60m = (file["data_60m"][ind[i]].reshape(1,20,20, 2)-arr_norm[2, 10:12])/arr_norm[3, 10:12]
             batch_20m = ndimage.zoom(batch_20m, [1,2,2,1], order=0)
             batch_60m = ndimage.zoom(batch_60m, [1,6,6,1], order=0)
             sample_x = np.concatenate((batch_10m,batch_20m,batch_60m), axis=3)
          else:
             #sample_x = (file[dataset][ind[i], :, :, 0:3].reshape(1,120,120, n_channels)-arr_norm[2, 0:3])/arr_norm[3, 0:3]
             sample_x = np.flip((file[dataset][ind[i], :, :, 0:3].reshape(1,120,120, n_channels)-arr_norm[2, 0:3])/arr_norm[3, 0:3], 3)
          i = i + 1
          yield(sample_x)

def test_generator_3branches(file, ind, arr_norm):
    i = 0
    d_10m="data_10m"
    d_20m="data_20m"
    d_60m="data_60m"
    while True:
          # Load test sample (paths/indices)
          sample_x_10m = np.empty([1,120,120, 4])
          sample_x_10m = (file[d_10m][ind[i]].reshape(1,120,120, 4)-arr_norm[2, 0:4])/arr_norm[3, 0:4]
          # Load test sample (paths/indices)
          sample_x_20m = np.empty([1,60,60, 6])
          sample_x_20m = (file[d_20m][ind[i]].reshape(1,60,60, 6)-arr_norm[2, 4:10])/arr_norm[3, 4:10]
          # Load test sample (paths/indices)
          sample_x_60m = np.empty([1,20,20, 2])
          sample_x_60m = (file[d_60m][ind[i]].reshape(1,20,20, 2)-arr_norm[2, 10:12])/arr_norm[3, 10:12]
          i = i + 1
          yield([sample_x_10m,sample_x_20m,sample_x_60m])


def get_y_true(file, ind):
    y_true = np.empty([0,43])
    for i in ind:
          y_true = np.vstack([y_true, file['classes'][i].reshape(1,43)]) 
    return y_true

def compute_input(path):

    mat_img = np.zeros([120,120,12])
    list_band = os.listdir(path) #list of bands
    i = 0
    for file in list_band: #for loop on the bands
        if file.endswith(".tif"):
            result = plt.imread(os.path.join(path, file))
            """
            if i == 4 or i == 5 or i == 6 or i ==9 or i==10 or i==11: #bands resolution 20m
                result = ndimage.zoom(load_img, 2, order=0)
            if i == 0 or i == 8:
                result = ndimage.zoom(load_img, 6, order=0)
    #order 0: Nearest-neighbor,1: Bi-linear (default),2: Bi-quadratic
    #3: Bi-cubic, 4: Bi-quartic, 5: Bi-quintic
            """
            lb_check = file.split("_")[5]
            if lb_check == "B05.tif" or lb_check == "B06.tif" or lb_check == "B07.tif" or lb_check == "B8A.tif" or \
            lb_check == "B11.tif" or lb_check == "B12.tif":
                #bands resolution 20m
                result = ndimage.zoom(result, 2, order=0)
            if lb_check == "B01.tif" or lb_check == "B09.tif":
                result = ndimage.zoom(result, 6, order=0)
            mat_img[:,:, i] = result
            i = i + 1
    return(mat_img)

def get_input(path):
     #function to load stacked matrix   
    mat_img = np.zeros([120,120,12])
    at_img = np.load(os.path.join(path, path.split("/")[2]) + '_mat.npy')
    
    return(mat_img)

def get_output(path, model_label):
    
    list_band = os.listdir(path)
    for file in list_band:
        if file.endswith(".json"):
            with open(os.path.join(path, file)) as json_file:  
                    labels = json.load(json_file)["labels"]
            lab_t = model_label.transform([labels])

    return(lab_t)

def preprocess_input(image):
    
    return(image)
    
def get_path2img(path):
    
    path2img = []
    i = 0
    list_dir = os.listdir(path)
    for l_d in list_dir:
        if l_d.split("_")[0] == "S2A" or l_d.split("_")[0] == "S2B":
            #path2img.append(l_d)
            path2img.append(os.path.join(path, l_d))
            i = i + 1
    return path2img, i
