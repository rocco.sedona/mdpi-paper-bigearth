#!/bin/bash -x
#SBATCH --nodes=8
#SBATCH --ntasks=32
#SBATCH --ntasks-per-node=4
#SBATCH --output=resnet_juwels_100e-out
#SBATCH --error=resnet_juwels_100e-err
#SBATCH --time=5:00:00
#SBATCH --gres=gpu:4 --partition=gpus

module load GCC/8.3.0  MVAPICH2/2.3.2-GDR

module load TensorFlow/1.13.1-GPU-Python-3.6.8
module load Horovod/0.16.2-GPU-Python-3.6.8
module load Keras/2.2.4-GPU-Python-3.6.8
module load scikit/2019a-Python-3.6.8
module load mpi4py/3.0.1-Python-3.6.8
 
# Activate virtualenv
#source ../.env_juwels/bin/activate

# Run the program

srun --cpu-bind=none python script_horovod_chunks_LARS_new.py -m 'super' -i '0' -e '100' -b '128' -w '5'
