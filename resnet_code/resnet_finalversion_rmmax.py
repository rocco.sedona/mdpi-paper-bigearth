# -*- coding: utf-8 -*-
'''ResNet50 model for Keras.
# Reference:
- [Deep Residual Learning for Image Recognition](https://arxiv.org/abs/1512.03385)
Adapted from code contributed by BigMoyan.
'''
from __future__ import print_function

import numpy as np
import warnings
from pathlib import Path

from keras.layers import Input
from keras import layers
from keras.layers import Dense
from keras.layers import Activation
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import GlobalMaxPooling2D
from keras.layers import ZeroPadding2D
from keras.layers import AveragePooling2D
from keras.layers import GlobalAveragePooling2D
from keras.layers import BatchNormalization
from keras.layers import Dropout
from keras.models import Model
from keras.regularizers import l2
from keras.preprocessing import image
from keras.layers import concatenate
import keras.backend as K
#from keras import backend as K
from keras.utils import layer_utils
from keras.utils.data_utils import get_file
from keras_applications.imagenet_utils import decode_predictions
from keras_applications.imagenet_utils import preprocess_input
from keras_applications.imagenet_utils import _obtain_input_shape
from keras.engine.topology import get_source_inputs


WEIGHTS_PATH = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5'
WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5'
weight_decay = 1.e-4

def identity_block(input_tensor, kernel_size, filters, stage, block):
    """The identity block is the block that has no conv layer at shortcut.
    # Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filterss of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Conv2D(filters1, (1, 1),  kernel_regularizer=l2(weight_decay), name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size, kernel_regularizer=l2(weight_decay), 
               padding='same', name=conv_name_base + '2b')(x)
    
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), kernel_regularizer=l2(weight_decay), name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    x = layers.add([x, input_tensor])
    x = Activation('relu')(x)
    return x


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    """conv_block is the block that has a conv layer at shortcut
    # Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filterss of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    Note that from stage 3, the first conv layer at main path is with strides=(2,2)
    And the shortcut should have strides=(2,2) as well
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Conv2D(filters1, (1, 1), strides=strides, kernel_regularizer=l2(weight_decay),
               name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size, padding='same', kernel_regularizer=l2(weight_decay),
               name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), kernel_regularizer=l2(weight_decay), name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    shortcut = Conv2D(filters3, (1, 1), strides=strides, kernel_regularizer=l2(weight_decay),
                      name=conv_name_base + '1')(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

    x = layers.add([x, shortcut])
    x = Activation('relu')(x)
    return x


def ResNet50(include_top=True, weights='imagenet',
             input_tensor=None, channels=3,
             pooling=None, setup_conf=None,
             classes=43):
    """Instantiates the ResNet50 architecture.
    Optionally loads weights pre-trained
    on ImageNet. Note that when using TensorFlow,
    for best performance you should set
    `image_data_format="channels_last"` in your Keras config
    at ~/.keras/keras.json.
    The model and the weights are compatible with both
    TensorFlow and Theano. The data format
    convention used by the model is the one
    specified in your Keras config file.
    # Arguments
	channels: option to select 12 or 3 channels
        include_top: whether to include the fully-connected
            layer at the top of the network.
        weights: one of `None` (random initialization)
            or "imagenet" (pre-training on ImageNet).
        input_tensor: optional Keras tensor (i.e. output of `layers.Input()`)
            to use as image input for the model.
        input_shape: optional shape tuple, only to be specified
            if `include_top` is False (otherwise the input shape
            has to be `(224, 224, 3)` (with `channels_last` data format)
            or `(3, 224, 244)` (with `channels_first` data format).
            It should have exactly 3 inputs channels,
            and width and height should be no smaller than 197.
            E.g. `(200, 200, 3)` would be one valid value.
        pooling: Optional pooling mode for feature extraction
            when `include_top` is `False`.
            - `None` means that the output of the model will be
                the 4D tensor output of the
                last convolutional layer.
            - `avg` means that global average pooling
                will be applied to the output of the
                last convolutional layer, and thus
                the output of the model will be a 2D tensor.
            - `max` means that global max pooling will
                be applied.
        classes: optional number of classes to classify images
            into, only to be specified if `include_top` is True, and
            if no `weights` argument is specified.
    # Returns
        A Keras model instance.
    # Raises
        ValueError: in case of invalid argument for `weights`,
            or invalid input shape.
    """
    if channels == 12:
        input_shape=(120,120,12)
    if channels == 3:
        input_shape=(120,120,3)

    if weights not in {'imagenet', None}:
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) or `imagenet` '
                         '(pre-training on ImageNet).')
    """
    if weights == 'imagenet' and include_top and classes != 1000:
        raise ValueError('If using `weights` as imagenet with `include_top`'
                         ' as true, `classes` should be 1000')
    """
    if weights == 'imagenet' and include_top and classes != 43:
        raise ValueError('If using `weights` as imagenet with `include_top`'
                         ' as true, `classes` should be 43')
    # Determine proper input shape
    # The image has to be 224x224x3 if we want to use the pre-trained net, correct?
    # Otherwise we have to train from scratch
    input_shape = _obtain_input_shape(input_shape,
                                      default_size=120,
                                      min_size=120,
                                      data_format=K.image_data_format(),
                                      require_flatten=include_top)

    if input_tensor is None:
        img_input = Input(shape=input_shape)
    else:
        if not K.is_keras_tensor(input_tensor):
            img_input = Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    #remove maxpooling after first convolution
    if setup_conf == 'rem_mp':
        x = Conv2D(64, (7, 7), strides=(2, 2), kernel_regularizer=l2(weight_decay), name='conv1')(img_input)
        x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
        x = Activation('relu')(x)
    #adapt first convolution
    elif setup_conf == 'ad_conv':
        x = Conv2D(64, (5, 5), strides=(1, 1), kernel_regularizer=l2(weight_decay), name='conv1')(img_input)
        x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
        x = Activation('relu')(x)
        x = MaxPooling2D((2, 2), strides=(1, 1))(x)
    else:
        x = ZeroPadding2D((3, 3))(img_input)
        x = Conv2D(64, (7, 7), strides=(2, 2), kernel_regularizer=l2(weight_decay), name='conv1')(x)
        x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
        x = Activation('relu')(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)
	

    #in this case we have 256 filters, correct?
    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

    x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

    x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

    x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')
    #modified size of window from (7,7) to (2,2)
    x = AveragePooling2D((2, 2), name='avg_pool_mod')(x)

    if include_top:
        x = Flatten()(x)
        #x = Dense(classes, activation='softmax', name='fc1000')(x)
        x = Dropout(rate=0.5)(x)
        x = Dense(classes, activation='sigmoid', name='fc43')(x)
    else:
        if pooling == 'avg':
            x = GlobalAveragePooling2D()(x)
        elif pooling == 'max':
            x = GlobalMaxPooling2D()(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = get_source_inputs(input_tensor)
    else:
        inputs = img_input
    # Create model.
    model = Model(inputs, x, name='resnet50')
    print(model.summary())
    # load weights
    if weights == 'imagenet':
        if include_top:
            weights_path = get_file('resnet50_weights_tf_dim_ordering_tf_kernels.h5',
                                    WEIGHTS_PATH,
                                    cache_subdir='models',
                                    md5_hash='a7b3fe01876f51b976af0dea6bc144eb')
            #weights_path = "~/.keras/models/resnet50_weights_tf_dim_ordering_tf_kernels.h5"
        else:
            weights_path = get_file('resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models',
                                    md5_hash='a268eb855778b3df3c7506639542a6af')
        model.load_weights(weights_path, by_name=True)
        #freeze layers except last 3, commented parts for testing trainability
        #print(model.summary())
        """
        for layer in model.layers:
           #print(layer.name)
           #print(type(layer.name))
           
           if layer.name != 'avg_pool_mod' and layer.name != 'fc43' and layer.name != 'flatten_1':
              #print(layer.name)
              layer.trainable = False
           else:
              layer.trainable = True
              #print(layer.name)
              #print("in True condition")
        """
        #freeze layers except last 5, commented parts for testing trainability
        for layer in model.layers[:-2]:
           layer.trainable = False
        #checking layers trainability
        for layer in model.layers:
           print(layer.trainable)
        
        if K.backend() == 'theano':
            layer_utils.convert_all_kernels_in_model(model)

        if K.image_data_format() == 'channels_first':
            if include_top:
                maxpool = model.get_layer(name='avg_pool_mod')
                shape = maxpool.output_shape[1:]
                dense = model.get_layer(name='fc43')
                layer_utils.convert_dense_weights_data_format(dense, shape, 'channels_first')

            if K.backend() == 'tensorflow':
                warnings.warn('You are using the TensorFlow backend, yet you '
                              'are using the Theano '
                              'image data format convention '
                              '(`image_data_format="channels_first"`). '
                              'For best performance, set '
                              '`image_data_format="channels_last"` in '
                              'your Keras config '
                              'at ~/.keras/keras.json.')
    return model

#3branches
def ResNet50_3branches(include_top=True, weights=None,
             input_tensor=None,
             pooling=None,
             classes=43):
    """Instantiates the ResNet50 architecture.
    Optionally loads weights pre-trained
    on ImageNet. Note that when using TensorFlow,
    for best performance you should set
    `image_data_format="channels_last"` in your Keras config
    at ~/.keras/keras.json.
    The model and the weights are compatible with both
    TensorFlow and Theano. The data format
    convention used by the model is the one
    specified in your Keras config file.
    # Arguments
	channels: option to select 12 or 3 channels
        include_top: whether to include the fully-connected
            layer at the top of the network.
        weights: one of `None` (random initialization)
            or "imagenet" (pre-training on ImageNet).
        input_tensor: optional Keras tensor (i.e. output of `layers.Input()`)
            to use as image input for the model.
        input_shape: optional shape tuple, only to be specified
            if `include_top` is False (otherwise the input shape
            has to be `(224, 224, 3)` (with `channels_last` data format)
            or `(3, 224, 244)` (with `channels_first` data format).
            It should have exactly 3 inputs channels,
            and width and height should be no smaller than 197.
            E.g. `(200, 200, 3)` would be one valid value.
        pooling: Optional pooling mode for feature extraction
            when `include_top` is `False`.
            - `None` means that the output of the model will be
                the 4D tensor output of the
                last convolutional layer.
            - `avg` means that global average pooling
                will be applied to the output of the
                last convolutional layer, and thus
                the output of the model will be a 2D tensor.
            - `max` means that global max pooling will
                be applied.
        classes: optional number of classes to classify images
            into, only to be specified if `include_top` is True, and
            if no `weights` argument is specified.
    # Returns
        A Keras model instance.
    # Raises
        ValueError: in case of invalid argument for `weights`,
            or invalid input shape.
    """
    input_shapeA=(120,120,4)
    input_shapeB=(60,60,6)
    input_shapeC=(20,20,2)
    # Determine proper input shape
    input_shapeA = _obtain_input_shape(input_shapeA,
                                      default_size=120,
                                      min_size=120,
                                      data_format=K.image_data_format(),
                                      require_flatten=include_top)
    input_shapeB = _obtain_input_shape(input_shapeB,
                                      default_size=60,
                                      min_size=60,
                                      data_format=K.image_data_format(),
                                      require_flatten=include_top)
    input_shapeC = _obtain_input_shape(input_shapeC,
                                      default_size=20,
                                      min_size=20,
                                      data_format=K.image_data_format(),
                                      require_flatten=include_top)

    img_inputA = Input(shape=input_shapeA)

    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1

    x = ZeroPadding2D((3, 3))(img_inputA)
    x = Conv2D(64, (7, 7), strides=(2, 2), name='conv1a')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv1a')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2))(x)

    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a1', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b1')
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='c1')

    x = conv_block(x, 3, [128, 128, 512], stage=3, block='a1')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b1')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c1')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='d1')

    x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a1')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b1')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c1')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d1')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e1')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f1')

    x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a1')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b1')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c1')
    #modified size of window from (7,7) to (2,2)
    x = AveragePooling2D((2, 2), name='avg_pool_moda')(x)
    x = Flatten()(x)
    x = Model(inputs=img_inputA, outputs=x, name='resnet50A')
    
    #second branch
    img_inputB = Input(shape=input_shapeB)

    y = ZeroPadding2D((3, 3))(img_inputB)
    y = Conv2D(64, (7, 7), strides=(2, 2), name='conv1b')(y)
    y = BatchNormalization(axis=bn_axis, name='bn_conv1b')(y)
    y = Activation('relu')(y)
    y = MaxPooling2D((3, 3), strides=(2, 2))(y)

    y = conv_block(y, 3, [64, 64, 256], stage=2, block='a2', strides=(1, 1))
    y = identity_block(y, 3, [64, 64, 256], stage=2, block='b2')
    y = identity_block(y, 3, [64, 64, 256], stage=2, block='c2')

    y = conv_block(y, 3, [128, 128, 512], stage=3, block='a2')
    y = identity_block(y, 3, [128, 128, 512], stage=3, block='b2')
    y = identity_block(y, 3, [128, 128, 512], stage=3, block='c2')
    y = identity_block(y, 3, [128, 128, 512], stage=3, block='d2')

    y = conv_block(y, 3, [256, 256, 1024], stage=4, block='a2')
    y = identity_block(y, 3, [256, 256, 1024], stage=4, block='b2')
    y = identity_block(y, 3, [256, 256, 1024], stage=4, block='c2')
    y = identity_block(y, 3, [256, 256, 1024], stage=4, block='d2')
    y = identity_block(y, 3, [256, 256, 1024], stage=4, block='e2')
    y = identity_block(y, 3, [256, 256, 1024], stage=4, block='f2')

    y = conv_block(y, 3, [512, 512, 2048], stage=5, block='a2')
    y = identity_block(y, 3, [512, 512, 2048], stage=5, block='b2')
    y = identity_block(y, 3, [512, 512, 2048], stage=5, block='c2')
    #modified size of window from (7,7) to (2,2)
    y = AveragePooling2D((2, 2), name='avg_pool_modb')(y)
    y = Flatten()(y)
    y = Model(inputs=img_inputB, outputs=y, name='resnet50B')

    #third branch
    img_inputC = Input(shape=input_shapeC)

    z = ZeroPadding2D((3, 3))(img_inputC)
    z = Conv2D(64, (7, 7), strides=(2, 2), name='conv1c')(z)
    z = BatchNormalization(axis=bn_axis, name='bn_conv1c')(z)
    z = Activation('relu')(z)
    z = MaxPooling2D((3, 3), strides=(2, 2))(z)

    z = conv_block(z, 3, [64, 64, 256], stage=2, block='a3', strides=(1, 1))
    z = identity_block(z, 3, [64, 64, 256], stage=2, block='b3')
    z = identity_block(z, 3, [64, 64, 256], stage=2, block='c3')

    z = conv_block(z, 3, [128, 128, 512], stage=3, block='a3')
    z = identity_block(z, 3, [128, 128, 512], stage=3, block='b3')
    z = identity_block(z, 3, [128, 128, 512], stage=3, block='c3')
    z = identity_block(z, 3, [128, 128, 512], stage=3, block='d3')

    z = conv_block(z, 3, [256, 256, 1024], stage=4, block='a3')
    z = identity_block(z, 3, [256, 256, 1024], stage=4, block='b3')
    z = identity_block(z, 3, [256, 256, 1024], stage=4, block='c3')
    z = identity_block(z, 3, [256, 256, 1024], stage=4, block='d3')
    z = identity_block(z, 3, [256, 256, 1024], stage=4, block='e3')
    z = identity_block(z, 3, [256, 256, 1024], stage=4, block='f3')

    z = conv_block(z, 3, [512, 512, 2048], stage=5, block='a3')
    z = identity_block(z, 3, [512, 512, 2048], stage=5, block='b3')
    z = identity_block(z, 3, [512, 512, 2048], stage=5, block='c3')
    #modified size of window from (7,7) to (1,1)
    z = AveragePooling2D((1, 1), name='avg_pool_modc')(z)
    z = Flatten()(z)
    z = Model(inputs=img_inputC, outputs=z, name='resnet50C')

    #concatenate 3 branches
    combined = concatenate([x.output, y.output, z.output])
    tbl = Dense(classes, activation='sigmoid', name='fc43')(combined)

    # Create model.
    model = Model(inputs=[x.input, y.input, z.input], outputs=tbl, name='resnet50_3branches')

    return model
